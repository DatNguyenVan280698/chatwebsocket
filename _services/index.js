const axios = require('axios');

const APITask = "http://morning-oasis-29841.herokuapp.com"
// Make a request for a user with a given ID
module.exports = {
	getTask,
	getProject,
  getRecurrent,
  getDepartment
}

function getTask() {
	return axios({
    method : "GET",
    url : APITask + "/api/procedure",

  }).then(res => res)
  .catch(err => {throw err})
}

function getProject() {
	return axios({
    method : "GET",
    url : "http://3.1.20.54/v1/projects",

  }).then(res => res)
  .catch(err => {throw err})
}

function getRecurrent() {
  return axios({
    method : "GET",
    url : "https://falling-frog-38743.pktriot.net/api/recurrent-tasks/",

  }).then(res => res)
  .catch(err => {throw err})
}

function getDepartment() {
  return axios({
    method : "GET",
    url : "https://dsd15-log.azurewebsites.net/Departments/getall/?member",

  }).then(res => res)
  .catch(err => {throw err})
}