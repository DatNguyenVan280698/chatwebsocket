var express= require('express');
var app = express();
var mysql = require('mysql');
var webSocketsServerPort = 1337;
// websocket and http servers
var webSocketServer = require('websocket').server;
var http = require('http');
var session = require('express-session');
var bodyParser = require('body-parser');
app.use(express.static('public'))

var services = require('./_services')
var connectionDB = mysql.createConnection({
  host     : 'remotemysql.com',
  user     : '5LGkbNk4z6',
  password : 'v6VDEdQLtD',
  database : '5LGkbNk4z6'
});

app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));
var task = [];
var id_task = [];

var project = [];
var id_project = [];

var recurrent = [];
var id_recurrent = [];

var department = [];
var id_department = [];

connectionDB.query('SELECT * FROM `group`', function(error, results, fields) {
  if(error) {
    console.log(error)
  }
  if (results.length > 0) {
    results.map(function(data, i) {
      if(data.id_task > 0) {
         task.push(data);
         id_task.push(data.id_task)
      }

      if(data.id_task == -1) {
        project.push(data);
        id_project.push(data.id_project)
      }

      if(data.id_task == -2) {
        recurrent.push(data);
        id_recurrent.push(data.id_project)
      }

      if(data.id_task == -3) {
        department.push(data);
        id_department.push(data.id_project)
      }
    })
  } 
});

//API Task
function schedulingJob() {
  console.log("Scheduling Job")
services.getTask()
  .then(res => {
    res.data.map(function(data, i) {
      if(id_task.indexOf(data.id) == -1) {
        id_task.push(data.id);
        // console.log(`INSERT INTO \`group\` (\`id_task\`,\`id_project\`, \`title\`, \`description\`, \`user_created\`, \`date_created\`,  \`deadline\`) VALUES ('${data.id}', '', '${data.title}','${data.content}',${data.added_by},${new Date(data.created_at).getTime()}, 0)`)
        connectionDB.query(`INSERT INTO \`group\` (\`id_task\`,\`id_project\`, \`title\`, \`description\`, \`user_created\`, \`date_created\`,  \`deadline\`) VALUES ('${data.id}', "", '${data.title}','${data.content}',${data.added_by},${new Date(data.created_at).getTime()}, 0)`, function(error, results, fields) {
          task.push({
            id: results.insertId,
            id_task : data.id,
            id_project : "",
            title : data.title,
            description : data.content,
            user_created : data.added_by,
            date_created : new Date(data.created_at).getTime(),
            deadline : 0,
          })

          // ????
        });
      }
    })
  })
setTimeout(function(){
services.getProject()
  .then(res => {
    console.log(res.data.results.length)
    res.data.results.map(function(data, i) {
      if(id_project.indexOf(data.id) == -1) {
        id_project.push(data.id);
        console.log(`INSERT INTO \`group\` (\`id_task\`,\`id_project\`, \`title\`, \`description\`, \`user_created\`, \`date_created\`,  \`deadline\`) VALUES ('-1', '${data.id}', '${data.name}','${data.description}',${data.created_by},${data.created_time}, ${data.deadline})`)
        connectionDB.query(`INSERT INTO \`group\` (\`id_task\`,\`id_project\`, \`title\`, \`description\`, \`user_created\`, \`date_created\`,  \`deadline\`) VALUES ('-1', '${data.id}', '${data.name}','${data.description}',${data.created_by},${data.created_time}, ${data.deadline})`, function(error, results) {
          project.push({
            id: results.insertId,
            id_task : -1,
            id_project : data.id,
            title : data.name,
            description : data.description,
            user_created : data.created_by,
            date_created : data.created_time,
            deadline : data.deadline,
          })
          // ????
        });
      }
    })
  })
}, 1000)

//recurrent
services.getRecurrent()
  .then(res => {
    res.data.map(function(data, i) {
      if(id_recurrent.indexOf(data._id) == -1) {
        id_recurrent.push(data._id);
        var sql = '';
        if(data.finish == null)
          sql = `INSERT INTO \`group\` (\`id_task\`,\`id_project\`, \`title\`, \`description\`, \`user_created\`, \`date_created\`,  \`deadline\`) VALUES ('-2', "${data._id}", '${data.name}','${data.description}','${data.creator.id}',${new Date(data.createdAt).getTime()}, '0')`
        else 
          sql = `INSERT INTO \`group\` (\`id_task\`,\`id_project\`, \`title\`, \`description\`, \`user_created\`, \`date_created\`,  \`deadline\`) VALUES ('-2', "${data._id}", '${data.name}','${data.description}','${data.creator.id}',${new Date(data.createdAt).getTime()}, '${new Date(data.finish).getTime()}')`

        connectionDB.query(sql, function(error, results, fields) {
          if(!error)
            recurrent.push({
              id: results.insertId,
              id_task : -2,
              id_project : data._id,
              title : data.name,
              description : data.description,
              user_created : data.creator.id,
              date_created : new Date(data.createdAt).getTime(),
              deadline : data.finish == null ? 0 : new Date(data.finish).getTime(),
            })

          // ????
        });
      }
    })
  })

//department
services.getDepartment()
  .then(res => {
    res.data.map(function(data, i) {
      if(id_department.indexOf(data._id) == -1) {
        console.log(data._id)
        id_department.push(data._id);
        var sql = '';
        // if(data.finish == null)
          sql = `INSERT INTO \`group\` (\`id_task\`,\`id_project\`, \`title\`, \`description\`, \`user_created\`, \`date_created\`,  \`deadline\`) VALUES ('-3', "${data._id}", '${data.depart_name}','${data.depart_des}','0', 0 , '0')`
        // else 
          // sql = `INSERT INTO \`group\` (\`id_task\`,\`id_project\`, \`title\`, \`description\`, \`user_created\`, \`date_created\`,  \`deadline\`) VALUES ('-2', "${data._id}", '${data.name}','${data.description}','${data.creator.id}',${new Date(data.createdAt).getTime()}, '${new Date(data.finish).getTime()}')`

        connectionDB.query(sql, function(error, results, fields) {
          if(!error)
            department.push({
              id: results.insertId,
              id_task : -3,
              id_project : data._id,
              title : data.depart_name,
              description : data.depart_des,
              user_created : '0',
              date_created : 0,
              deadline : '0',
            })

          // ????
        });
      }
    })
  })
}
// schedulingJob();
setInterval(schedulingJob, 15000)
// setInterval(function(){
//   console.log(task)
// }, 10000)
// setInterval(function(){
  // console.log(task)
// }, 10000)
/**
 * Global variables
 */
// var history = [ ];

// connectionDB.query('SELECT * FROM history WHERE id_group = 1 ORDER BY time ASC', function(error, results, fields) {
//   if(error) {
//     console.log(error)
//   }
//   if (results.length > 0) {
//     results.map(function(data, i) {
//       data.time = parseInt(data.time);
//       history.push(data)
//     })
//   } 
// });
// list of currently connected clients (users)
var clients = [ ];
var id = [ ];

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.listen(8080, function(){
  console.log("Start port 8080")
})

app.get("/", function(req, res) {
  res.sendFile(__dirname + '/login.html')
})

app.get("/chat", function(req, res) {
  // if(req.session.loggin)
    res.sendFile(__dirname + '/index.html')
  // else 
    // res.redirect('/');
})
app.post('/login', function(request, response) {
  var username = request.body.username;
  var password = request.body.password;
  if (username && password) {
    connectionDB.query('SELECT * FROM user WHERE username = ? AND password = ?', [username, password], function(error, results, fields) {
      if (results.length > 0) {
        request.session.loggin = true;
        // id.push(results[0].id)
        response.status(200).send({username : username, name : results[0].name,id : results[0].id, email: results[0].email, password: password, type: 1})
      } else {
        response.status(200).send({message: "thông tin sai", type: 0})
      }     
      response.end();
    });
  } 
});

// app.get("/message", function(req, res) {
//   res.send({data : history});
//   res.end();
// })

app.get("/group/:id", function(req, res) {
  var message = [];
  connectionDB.query(`SELECT * FROM history WHERE id_group = ${req.params.id} ORDER BY time ASC`, function(error, results, fields) {
    if(error) {
      console.log(error)
    }
    if (results.length > 0) {
      results.map(function(data, i) {
        data.time = parseInt(data.time);
        message.push(data)
      })
    } 
    res.send({data : message});
    res.end();
});
})

app.get("/group_task", function(req, res) {
  // var message = [];
  // connectionDB.query(`SELECT * FROM history WHERE id_group = ${req.params.id} ORDER BY time ASC`, function(error, results, fields) {
  //   if(error) {
  //     console.log(error)
  //   }
  //   if (results.length > 0) {
  //     results.map(function(data, i) {
  //       data.time = parseInt(data.time);
  //       message.push(data)
  //     })
  //   } 
  //   res.send({data : message});
  //   res.end();
  // });
  res.send({task : task})
})

app.get("/group_project", function(req, res) {
  res.send({project : project})
})

app.get("/group_recurrent", function(req, res) {
  res.send({recurrent : recurrent})
})

app.get("/group_department", function(req, res) {
  res.send({department : department})
})

app.get("/group_all", function(req, res) {
  res.send({
    task : task,
    project : project,
    project : project,
    department : department,
  })
})

app.get("/message/:id", function(req, res) {
  var message = [];
  connectionDB.query(`SELECT * FROM history WHERE id = ${req.params.id} ORDER BY time ASC`, function(error, results, fields) {
    if(error) {
      console.log(error)
    }
    if (results.length > 0) {
      results.map(function(data, i) {
        data.time = parseInt(data.time);
        message.push(data)
      })
    } 
    res.send({data : message});
    res.end();
});
})

app.delete("/message/", function(req, res) {
  if(req.body.id == null || req.body.time == null) {
      res.send({ status : "Error"})
  }
  else
    connectionDB.query(`DELETE FROM history WHERE id = ${req.body.id} AND time = "${req.body.time}"`, function(error, results, fields) {
      if(!error) {
        // history = []
        // connectionDB.query('SELECT * FROM history ORDER BY time ASC', function(error, results, fields) {
        //   if(error) {
        //     console.log(error)
        //   }
        //   if (results.length > 0) {
        //     results.map(function(data, i) {
        //       data.time = parseInt(data.time);
        //       history.push(data)
        //     })
        //   } 
        // });
        res.send({ status : "Delete message OK"})
      }
      else {
        res.send({ status : "Error"})
      }

       res.end();
    });
 
})

app.put("/message/", function(req, res) {
  console.log(req.body)
  if(req.body.id == null || req.body.time == null || req.body.text == null) {
      res.send({ status : "Incomplete data"})
  }
  else
    connectionDB.query(`UPDATE history SET text="${req.body.text}" WHERE id = ${req.body.id} AND time = "${req.body.time}"`, function(error, results, fields) {
      if(!error) {
        res.send({ status : "Update message OK"})
      }
      else {
        console.log(error)
        res.send({ status : "Error"})
      }

       res.end();
    });
 
})

function htmlEntities(str) {
  return String(str)
      .replace(/&/g, '&amp;').replace(/</g, '&lt;')
      .replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
/**
 * HTTP server
 */
var server = http.createServer(function(request, response) {
  // Not important for us. We're writing WebSocket server,
  // not HTTP server
});
server.listen(webSocketsServerPort, function() {
  console.log((new Date()) + " Server is listening on port "
      + webSocketsServerPort);
});
/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
  // WebSocket server is tied to a HTTP server. WebSocket
  // request is just an enhanced HTTP request. For more info 
  // http://tools.ietf.org/html/rfc6455#page-6
  httpServer: server
});
// This callback function is called every time someone
// tries to connect to the WebSocket server
wsServer.on('request', function(request) {
  console.log((new Date()) + ' Connection from origin '
      + request.origin + '.');
  // accept connection - you should check 'request.origin' to
  // make sure that client is connecting from your website
  // (http://en.wikipedia.org/wiki/Same_origin_policy)
  var connection = request.accept(null, request.origin); 
  // we need to know client index to remove them on 'close' event
  var index = clients.push(connection) - 1;
  var userName = false;
  var id = null;
  var userColor = false;
  console.log((new Date()) + ' Connection accepted.');
  // send back chat history
  // if (history.length > 0) {
    connection.sendUTF(
        JSON.stringify({ type: 'history'} ));
  // }
  // user sent some message
  connection.on('connection', function() {
    console.log("Connect Server")
  })
  connection.on('open', function() {
    console.log("Open")
  })
  connection.on('message', function(message) {
    console.log(message)
    if (message.type === 'utf8') { // accept only text
    // first message sent by user is their name
     if (userName === false) {
        // remember user name
        var data = JSON.parse(message.utf8Data);
        userName = htmlEntities(data.name);
        id = data.id
        // get random color and send it back to the user
        userColor = "red";
        connection.sendUTF(
            JSON.stringify({ type:'color', data: userColor }));
        console.log((new Date()) + ' User is known as: ' + userName
                    + ' with ' + userColor + ' color.');
      } else { // log and broadcast the message
        console.log((new Date()) + ' Received Message from '
                    + userName + ': ' + message.utf8Data);
        var data = JSON.parse(message.utf8Data);
        console.log(data)
        message = data.message;
        // connectionDB.query(`SELECT * FROM \`group\` WHERE id_task = ${data.id_task} AND id_project = '${data.id_project}'`, function(err, result) {
        //   if(!err) {
        //     if(result.length > 0) {
        //       console.log(result[0])
        //       var obj = {
        //         id: id,
        //         time: (new Date()).getTime(),
        //         text: htmlEntities(message),
        //         author: userName,
        //         color: userColor,
        //         id_group: result[0].id
        //       };
        //       // console.log(`INSERT INTO history (id, time, text, author, color, id_group) VALUES ( ${obj.id}, '${obj.time.toString()}', '${obj.text}', '${obj.author}', '${obj.color}', ${result.id})`)
        //       connectionDB.query(`INSERT INTO history (id, time, text, author, color, id_group) VALUES ( ${obj.id}, '${obj.time.toString()}', '${obj.text}', '${obj.author}', '${obj.color}', ${result[0].id})`, function(error, results, fields) {
        //       });
        //       // history.push(obj);
        //       // history = history.slice(-100);
        //       // broadcast message to all connected clients
        //       var json = JSON.stringify({ type:'message', data: obj });
        //       for (var i=0; i < clients.length; i++) {
        //         clients[i].sendUTF(json);
        //       }
        //     }
        //   }
        //   else console.log(err)
        // })


        var id_group = data.id_group;
        // we want to keep history of all sent messages
        var obj = {
          id: id,
          time: (new Date()).getTime(),
          text: htmlEntities(message),
          author: userName,
          color: userColor,
          id_group: id_group
        };
        console.log(`INSERT INTO history (id, time, text, author, color) VALUES ( ${obj.id}, '${obj.time.toString()}', '${obj.text}', '${obj.author}', '${obj.color}')`)
        connectionDB.query(`INSERT INTO history (id, time, text, author, color, id_group) VALUES ( ${obj.id}, '${obj.time.toString()}', '${obj.text}', '${obj.author}', '${obj.color}', ${id_group})`, function(error, results, fields) {
        });
        // history.push(obj);
        // history = history.slice(-100);
        // broadcast message to all connected clients
        var json = JSON.stringify({ type:'message', data: obj });
        for (var i=0; i < clients.length; i++) {
          clients[i].sendUTF(json);
        }
      }
    }
  });
  // user disconnected
  connection.on('close', function(connection) {
    if (userName !== false && userColor !== false) {
      console.log((new Date()) + " Peer "
          + connection + " disconnected.");
      // remove user from the list of connected clients
      clients.splice(index, 1);
      // id.splice(index, 1);
      // push back user's color to be reused by another user
    }
  });
});