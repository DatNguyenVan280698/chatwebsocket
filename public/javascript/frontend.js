$(function () {
    "use strict";
    // setInterval(function() {
    //     console.log(id_group)
    // }, 1000)

    // for better performance - to avoid searching in DOM
    var content = $('#content');
    var input = $('#input');
    var status = $('#status');
    // var id_group = 2;
    // my color assigned by the server
    var myColor = false;
    // my name sent to the server
    var myName = sessionStorage.name;

    // if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    // if browser doesn't support WebSocket, just show some notification and exit
    if (!window.WebSocket) {
        content.html($('<p>', { text: 'Sorry, but your browser doesn\'t '
                                    + 'support WebSockets.'} ));
        input.hide();
        $('span').hide();
        return;
    }

    // open connection
    // var websocketUrl = 'ws://13.67.61.152:1337'
    // var websocketUrl = 'ws://localhost:1337'
    var connection = new WebSocket(websocketUrl);

    connection.onopen = function () {
        // first we want users to enter their names
        console.log("Open")
        input.removeAttr('disabled');
        var name = sessionStorage.name;
        var id = sessionStorage.id;
        connection.send(JSON.stringify({name : name, id : id}))

    };

    connection.onerror = function (error) {
        // just in there were some problems with conenction...
        content.html($('<p>', { text: 'Sorry, but there\'s some problem with your '
                                    + 'connection or the server is down.' } ));
    };

    // most important part - incoming messages
    connection.onmessage = function (message) {
        // try to parse JSON message. Because we know that the server always returns
        // JSON this should work without any problem but we should make sure that
        // the massage is not chunked or otherwise damaged.
        try {
            var json = JSON.parse(message.data);
            console.log(json)
        } catch (e) {
            console.log('This doesn\'t look like a valid JSON: ', message.data);
            return;
        }

        // NOTE: if you're not sure about the JSON structure
        // check the server source code above
        if (json.type === 'color') { // first response from the server with user's color
            // myColor = json.data;
            console.log(myName)
            status.text(myName + ': ').css('color', myColor);
            input.removeAttr('disabled').focus();
            // from now user can start sending messages
        } else if (json.type === 'history') { // entire message history
            // insert every single message to the chat window
            var quizUrl = apiUrl + '/group/' + id_group;
            fetch(quizUrl,{
                mode: 'no-cors',
                method: 'get',
            }).then(function(response) {
                response.text().then(function(text) {
                    text = JSON.parse(text)
                    for (var i=0; i < text.data.length; i++) {
                    addMessage(text.data[i].author, text.data[i].text,
                               text.data[i].color, new Date(text.data[i].time), text.data[i].id);
                    }
                })
            }).catch(function(err) {
              console.log(err)
            });
            // getMessageIdGroup(1)
            //     .then(res => console.log(res))
            //     .catch(err => {throw  err})
            // for (var i=0; i < json.data.length; i++) {
            //     addMessage(json.data[i].author, json.data[i].text,
            //                json.data[i].color, new Date(json.data[i].time), json.data[i].id);
            // }
        } else if (json.type === 'message') { // it's a single message
            input.removeAttr('disabled'); // let the user write another message
            addMessage(json.data.author, json.data.text,
                       json.data.color, new Date(json.data.time), json.data.id);
        } else {
            console.log('Hmm..., I\'ve never seen JSON like this: ', json);
        }
    };

    /**
     * Send mesage when user presses Enter key
     */
    input.keydown(function(e) {
        if (e.keyCode === 13) {
            var msg = $(this).val();
            if (!msg) {
                return;
            }
            // send the message as an ordinary text
            connection.send(JSON.stringify({
                message : msg,
                id_group : id_group,
                id_task : id_task,
                id_project : id_project,
            }));
            $(this).val('');
            // disable the input field to make the user wait until server
            // sends back response
            // input.attr('disabled', 'disabled');

            // we know that the first message sent from a user their name
            if (myName === false) {
                myName = msg;
            }
        }
    });

    /**
     * This method is optional. If the server wasn't able to respond to the
     * in 3 seconds then show some error message to notify the user that
     * something is wrong.
     */
    setInterval(function() {
        if (connection.readyState !== 1) {
            status.text('Error');
            input.attr('disabled', 'disabled').val('Unable to comminucate '
                                                 + 'with the WebSocket server.');
        }
    }, 3000);
    function deleteMessage(id, dt) {
        console.log(id)
    }
    /**
     * Add message to the chat window
     */
    function addMessage(author, message, color, dt, id) {
      if(id == sessionStorage.id)
        content.append('<p style="text-align:right; color:blue"> @ ' +
             + (dt.getHours() < 10 ? '0' + dt.getHours() : dt.getHours()) + ':'
             + (dt.getMinutes() < 10 ? '0' + dt.getMinutes() : dt.getMinutes())
             + ': ' + message + `<button onclick="deleteMessage(${id}, ${dt.getTime()})">Del</button></p>`);
      else 
        content.append('<p><span style="color:' + color + '">' + author + '</span> @ ' +
             + (dt.getHours() < 10 ? '0' + dt.getHours() : dt.getHours()) + ':'
             + (dt.getMinutes() < 10 ? '0' + dt.getMinutes() : dt.getMinutes())
             + ': ' + message + '</p>');
        content.scrollTop(document.getElementById("content").scrollHeight);
    }

});
var id_group = 1;
var id_task = 0, id_project = "";
  function changeTask(x, task) {
    // location.reload()
    // console.log(connection)

    // ? get value ?
    id_group = task.id;
    id_task = task.id_task;
    id_project = task.id_project;
    for( var i = 0; i < document.getElementsByClassName("menu").length; i++) {
        if(i == (x))
          document.getElementsByClassName("menu")[i].className = "menu active";
        else 
          document.getElementsByClassName("menu")[i].className = "menu";
    }
    $("#title").html(task.title);
    $("#description").html(task.description);
    var content = $('#content');
    content.empty();

    var quizUrl = apiUrl + '/group/' + id_group;
    fetch(quizUrl,{
        mode: 'no-cors',
        method: 'get',
    }).then(function(response) {
        response.text().then(function(text) {
            text = JSON.parse(text)
            for (var i=0; i < text.data.length; i++) {
            startMessage(text.data[i].author, text.data[i].text,
                       text.data[i].color, new Date(text.data[i].time), text.data[i].id);
            }
        })
    }).catch(function(err) {
      console.log(err)
    });
  }
function changeProject(x, project) {
    // location.reload()
    // console.log(connection)

    // ? get value ?
    id_group = project.id;
    id_task = project.id_task;
    id_project = project.id_project;
    for( var i = 0; i < document.getElementsByClassName("menu-project").length; i++) {
        if(i == (x))
          document.getElementsByClassName("menu-project")[i].className = "menu-project active";
        else 
          document.getElementsByClassName("menu-project")[i].className = "menu-project";
    }
    $("#title").html(project.title);
    $("#description").html(project.description);
    var content = $('#content');
    content.empty();

    var quizUrl = apiUrl + '/group/' + id_group;
    fetch(quizUrl,{
        mode: 'no-cors',
        method: 'get',
    }).then(function(response) {
        response.text().then(function(text) {
            text = JSON.parse(text)
            for (var i=0; i < text.data.length; i++) {
            startMessage(text.data[i].author, text.data[i].text,
                       text.data[i].color, new Date(text.data[i].time), text.data[i].id);
            }
        })
    }).catch(function(err) {
      console.log(err)
    });
  }

function changeRecurrent(x, recurrent) {
    // location.reload()
    // console.log(connection)

    // ? get value ?
    id_group = recurrent.id;
    id_task = recurrent.id_task;
    id_project = recurrent.id_project;
    for( var i = 0; i < document.getElementsByClassName("menu-recurrent").length; i++) {
        if(i == (x))
          document.getElementsByClassName("menu-recurrent")[i].className = "menu-recurrent active";
        else 
          document.getElementsByClassName("menu-recurrent")[i].className = "menu-recurrent";
    }
    $("#title").html(recurrent.title);
    $("#description").html(recurrent.description);
    var content = $('#content');
    content.empty();

    var quizUrl = apiUrl + '/group/' + id_group;
    fetch(quizUrl,{
        mode: 'no-cors',
        method: 'get',
    }).then(function(response) {
        response.text().then(function(text) {
            text = JSON.parse(text)
            for (var i=0; i < text.data.length; i++) {
            startMessage(text.data[i].author, text.data[i].text,
                       text.data[i].color, new Date(text.data[i].time), text.data[i].id);
            }
        })
    }).catch(function(err) {
      console.log(err)
    });
  }

function changeDepartment(x, department) {
    // location.reload()
    // console.log(connection)

    // ? get value ?
    id_group = department.id;
    id_task = department.id_task;
    id_project = department.id_project;
    console.log(id_task)
    for( var i = 0; i < document.getElementsByClassName("menu-department").length; i++) {
        if(i == (x))
          document.getElementsByClassName("menu-department")[i].className = "menu-department active";
        else 
          document.getElementsByClassName("menu-department")[i].className = "menu-department";
    }
    $("#title").html(department.title);
    $("#description").html(department.description);
    var content = $('#content');
    content.empty();

    var quizUrl = apiUrl + '/group/' + id_group;
    fetch(quizUrl,{
        mode: 'no-cors',
        method: 'get',
    }).then(function(response) {
        response.text().then(function(text) {
            text = JSON.parse(text)
            for (var i=0; i < text.data.length; i++) {
            startMessage(text.data[i].author, text.data[i].text,
                       text.data[i].color, new Date(text.data[i].time), text.data[i].id);
            }
        })
    }).catch(function(err) {
      console.log(err)
    });
  }

async function deleteMessage(id, dt) {
     console.log({
            id : id,
            time : dt
        })
    //call API delete message
    // var url = 'http://13.67.61.152:8080/message';
    var url = apiUrl + '/message';
    const response = fetch(url, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
        body : JSON.stringify({
            id : id,
            time : dt
        })
    });
}

async function getMessageIdGroup(id) {
    //call API delete message
    // var url = 'http://13.67.61.152:8080/message';
    var url = apiUrl + '/message/' + id;
    const response = fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
    });
}

function startMessage(author, message, color, dt, id) {
    var content = $('#content');
      if(id == sessionStorage.id)
        content.append('<p style="text-align:right; color:blue"> @ ' +
             + (dt.getHours() < 10 ? '0' + dt.getHours() : dt.getHours()) + ':'
             + (dt.getMinutes() < 10 ? '0' + dt.getMinutes() : dt.getMinutes())
             + ': ' + message + `<button onclick="deleteMessage(${id}, ${dt.getTime()})">Del</button></p>`);
      else 
        content.append('<p><span style="color:' + color + '">' + author + '</span> @ ' +
             + (dt.getHours() < 10 ? '0' + dt.getHours() : dt.getHours()) + ':'
             + (dt.getMinutes() < 10 ? '0' + dt.getMinutes() : dt.getMinutes())
             + ': ' + message + '</p>');
        content.scrollTop(document.getElementById("content").scrollHeight);
    }